<?php

namespace App\Form;

use App\Entity\Creature;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Form\Type\SubmitType;

class CreatureType extends AbstractType
{

  /**
   * [buildForm description]
   * @param  FormBuilderInterface $builder [description]
   * @param  array                $options [description]
   */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('texteLead')
            ->add('texteSuite')
            ->add('dateCreation', Type\DateType::class, [
                  'widget' => 'single_text',
                  'format' => 'yyyy-MM-dd',
              ])
            ->add('image')
            ->add('slug')
            ->add('film', EntityType::class, [
              'class' => \App\Entity\Film::class,
              'choice_value' => 'id',
              'choice_label' => 'titre',
            ])
            ->add('tags', EntityType::class, [
                    'class' => \App\Entity\Tag::class,
                    'choice_value' => 'id',
                    'choice_label' => 'nom',
              		  'multiple' => true,
              		  'expanded' => true
                 ])
            ->add('submit', Type\SubmitType::class)
        ;
    }

/**
 * [configureOptions description]
 * @param  OptionsResolver $resolver [description]
 */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Creature::class,
        ]);
    }
}
