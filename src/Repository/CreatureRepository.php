<?php

namespace App\Repository;

use App\Entity\Creature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Creature|null find($id, $lockMode = null, $lockVersion = null)
 * @method Creature|null findOneBy(array $criteria, array $orderBy = null)
 * @method Creature[]    findAll()
 * @method Creature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreatureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Creature::class);
    }

/**
 * [findAllBySearch description]
 * @param  string $word [description]
 * @return array $query [description]
 */
      public function findAllBySearch($word)
    {
        $qb = $this->createQueryBuilder('c')
            ->select()
            ->join('c.film', 'f')
            ->join('c.tags', 't');
            foreach ($word as $id){
                $qb->orWhere($qb->expr()->like('c.nom', $qb->expr()->literal('%' . $id . '%')))
                   ->orWhere($qb->expr()->like('c.texteSuite', $qb->expr()->literal('%' . $id . '%')))
                   ->orWhere($qb->expr()->like('c.texteLead', $qb->expr()->literal('%' . $id . '%')))
                   ->orWhere($qb->expr()->like('f.titre', $qb->expr()->literal('%' . $id . '%')))
                   ->orWhere($qb->expr()->like('t.nom', $qb->expr()->literal('%' . $id . '%')));
            }

            $qb->orderBy('c.dateCreation', 'DESC');
            $query = $qb->getQuery();

        return $query->execute();
    }

}
