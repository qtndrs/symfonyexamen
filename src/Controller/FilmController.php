<?php
/*
  ./src/Controller/FilmController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Film;


class FilmController extends GenericController {

/**
 * [indexAction description]
 * @param  string $vue     [description]
 * @param  array  $orderBy [description]
 * @param  int $limit   [description]
 * @return array $films [description]
 */
    public function indexAction(string $vue = 'index', array $orderBy = ['titre' => 'DESC'] ,int $limit = null) {
      $films = $this->_repository->findBy([], $orderBy, $limit);
      return $this->render('films/'.$vue.'.html.twig',[
        'films' => $films
      ]);
    }

}
