<?php
/*
  ./src/Controller/CreatureController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Creature;
use App\Form\CreatureType;

/**
 * [CreatureController extends GenericController]
 */
class CreatureController extends GenericController {

/**
 * [indexAction]
 * @param  string $vue
 * @param  array  $orderBy
 * @param  int $limit
 * @return array $creatures
 */
public function indexAction(string $vue = 'index', array $orderBy = ['dateCreation' => 'DESC'] ,int $limit = null){
      $creatures = $this->_repository->findBy([], $orderBy, $limit);
      return $this->render('creatures/'.$vue.'.html.twig',[
        'creatures' => $creatures
      ]);
    }


/**
 * [searchAction]
 * @param  Request $request
 * @return array $creatures
 * @return string $search
 */
public function searchAction(Request $request){
    $search = $request->query->get('search');
    $word = explode(' ', $search);
    $creatures = $this->_repository->findAllBySearch($word);
    return $this->render('creatures/search.html.twig', [
      'creatures' => $creatures,
      'search' => $search
    ]);
  }

/**
 * [addAction]
 * @param Request $request
 *
 */
public function addAction(Request $request){
      $creature = new Creature();
      $form = $this->createForm(CreatureType::class, $creature);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($creature);
        $manager->flush();
        $this->get('session')->getFlashBag()->clear();
        $this->addFlash('message', "La créature a bien été ajoutée");
        return $this->redirectToRoute('app_pages_show', ['id' => 3, 'slug' => 'creatures']);
      }
      return $this->render('creatures/add.html.twig', [
        	'creature' => $creature,
          'form' => $form->createView()
        ]);
    }

/**
 * [editAction]
 * @param  Creature $creature
 * @param  Request  $request
 * @return array $creature 
 */
public function editAction(Creature $creature, Request $request){
      $form = $this->createForm(CreatureType::class, $creature);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $manager = $this->getDoctrine()->getManager();
        $manager->flush();
        $this->get('session')->getFlashBag()->clear();
        $this->addFlash('message', "La créature a bien été modifiée");
        return $this->redirectToRoute('app_pages_show', ['id' => 3, 'slug' => 'creatures']);
      }

      return $this->render('creatures/edit.html.twig', [
        	'creature' => $creature,
          'form' => $form->createView()
        ]);

    }



/**
 * [deleteAction]
 * @param  Creature $creature
 * @param  Request  $request
 * @return array [retourne un tableau composé d'un ID et d'un slug]
 */
public function deleteAction(Creature $creature, Request $request){
      if ($this->isCsrfTokenValid('delete'.$creature->getId(), $request->get('_token'))){
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($creature);
        $manager->flush();
        $this->get('session')->getFlashBag()->clear();
        $this->addFlash('message', "La créature a bien été supprimée");
      }
      return $this->redirectToRoute('app_pages_show', ['id' => 3, 'slug' => 'creatures']);
    }
}
