<?php
/*
    ./src/Controller/TestController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Page;
use Symfony\Component\HttpFoundation\Request;

/**
 * [PageController extends GenericController]
 */
class PageController extends GenericController {

  /**
   * [showAction]
   * @param  int     $id      [description]
   * @param  Request $request [description]
   * @return array $page          [description]
   */
    public function showAction(int $id, Request $request) {
        $page = $this->_repository->find($id);

        return $this->render('pages/show.html.twig', [
          'page' => $page
        ]);
    }

/**
 * [indexAction description]
 * @return array $pages [description]
 */
    public function indexAction(){
    $pages = $this->_repository->findAll();
    return $this->render('pages/index.html.twig',[
      'pages' => $pages
    ]);
  }

}
