<?php
/*
  ./src/Controller/TagController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Tag;
use Symfony\Component\HttpFoundation\Request;


/**
 * [TagController extends GenericController]
 */
class TagController extends GenericController {

/**
 * [indexAction description]
 * @param  integer $limit [description]
 * @return array $tags        [description]
 */
    public function indexAction(int $limit = 10){
      $tags = $this->_repository->findBy(
                                  [],
                                  ['nom' => 'ASC'],
                                  $limit
                                );
      return $this->render('tags/index.html.twig',[
        'tags' => $tags
      ]);
    }

/**
 * [showAction description]
 * @param  int     $id      [description]
 * @param  Request $request [description]
 * @return array $tag           [description]
 */
    public function showAction(int $id, Request $request) {
        $tag = $this->_repository->find($id);

        return $this->render('tags/show.html.twig', [
          'tag' => $tag
        ]);
    }
}
